package hack.android.conferencebarcode.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class MainActivity extends ActionBarActivity {
    String apikey = "GTqg08fjeb";
    String scanresult = null;
    String myid = null;

// create a method to handle the input of user data

    public void onActivityResult(int requestCode, int resultCode, Intent intent){

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        Context context = getApplicationContext();
        if (scanResult != null){
//        send the result to the server with the user email /signup details
            String scanContent = scanResult.getContents();
//            String scanFormat = scanResult.getFormatName();
            Toast toast = Toast.makeText(context, scanContent, 5000);
            toast.show();
            final TextView textViewToChange = (TextView) findViewById(R.id.scanid);
//            textViewToChange.setText(scanContent);
            scanresult = scanContent;
        }
        else {
            Toast toast = Toast.makeText(context, "Error, returns null", 5000);
            toast.show();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void startScan (View v){
        if(v.getId()==R.id.scanbutton){
            Toast.makeText(getApplicationContext(),"button clicked", 3000).show();
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }
        else if(v.getId()==R.id.secondscanbutton){
            Toast.makeText(getApplicationContext(),"button clicked main scan", 3000).show();
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
            Toast toast = Toast.makeText(getApplicationContext(), "Scan Pair Submitted, wait for the email!!", 5000);
            toast.show();
        }
        else
            Toast.makeText(getApplicationContext(),"error", 3000).show();
    }

    public void submitRegData (View v){
        String name;
        String email;
        String userbarcode;
        AsyncTask response;
        EditText nameEdit = (EditText) findViewById(R.id.editText);
        EditText emailEdit = (EditText) findViewById(R.id.editText2);
        TextView barEdit = (TextView) findViewById(R.id.scanid);

        name = nameEdit.getText().toString();
        email = emailEdit.getText().toString();
        userbarcode = barEdit.getText().toString();

        RegPost regpost = new RegPost();
        response = regpost.execute(name,email,userbarcode,apikey);
//if the post completes change view to scan screen, persist values
        if (response != null){
            setContentView(R.layout.scanning_screen);
//            save values to memory for later retrieval
            SharedPreferences sharedPref = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(name,name);
            editor.putString(email,email);
            editor.putString(userbarcode,userbarcode);
            editor.commit();
            myid = name;
        }

    }

    public void submitConnectData(View v){

        AsyncTask response;
// submit http get request of the scanned barcode and check the response.
//        get the parameters from the memory and the scanned code.
//        SharedPreferences sharedPref = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        sharedPref.getString("userbarcode",null);

        String userid = sharedPref.getString("name",null);
        RegGet regget = new RegGet();
        response = regget.execute(userid,scanresult,apikey);
        if (response != null){
//            if the request is successful show a success toast and return to minimise the app, if not show error message


        }



    }

    public class RegGet extends AsyncTask<String, Void, String>{
        public String doInBackground(String... getParams){
            HttpClient httpClient = new DefaultHttpClient();
			Log.d("response", "args ready");
            Log.d("data", getParams[0]);
            Log.d("data", getParams[1]);
            HttpResponse response= null;
            HttpGet httpGet = new HttpGet("https://scanmyidentity.appspot.com/api/connect/?yourid="+137393+"&theirid="+getParams[1]+"&apikey="+apikey);
//				try {
//					httpGet.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//					Log.d("response", "request ready");
//				} catch (UnsupportedEncodingException e) {
//					e.printStackTrace();
//				}
            try {
                response = httpClient.execute(httpGet);
                Log.d("data", httpGet.toString());
                Log.d("response", response.toString());
                Log.d("data", httpGet.toString());
            } catch (ClientProtocolException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            }
            return response.toString();
        }
        protected void onGetExecute() {
            Toast.makeText(MainActivity.this, "success http",
                    Toast.LENGTH_LONG).show();
        }
    }

    public class RegPost extends AsyncTask<String, Void, String>{
        public String doInBackground(String... params){
            HttpClient httpClient = new DefaultHttpClient();
//				HttpGet httpGet = new HttpGet("http://5.79.21.67/process.php");
//				String userAgent = System.getProperty("http-agent");
//				httpGet.setHeader("User-Agent", userAgent);
//				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
//				nameValuePairs.add(new BasicNameValuePair("mode", "4"));
//				nameValuePairs.add(new BasicNameValuePair("lesson", lessonName[0]));
//				Log.d("response", "args ready");
//            Log.d("data", lessonName[0]);
//            Log.d("data", lessonName[1]);
            HttpResponse response= null;
            HttpPost httpPost = new HttpPost("https://scanmyidentity.appspot.com/api/create-account/");
//				try {
//					httpGet.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//					Log.d("response", "request ready");
//				} catch (UnsupportedEncodingException e) {
//					e.printStackTrace();
//				}
            try {
//                name and email hardcoded
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(4);
                nameValuePairList.add(new BasicNameValuePair("name","newname"));
                nameValuePairList.add(new BasicNameValuePair("email","email@email.com"));
                nameValuePairList.add(new BasicNameValuePair("userid",scanresult));
                nameValuePairList.add(new BasicNameValuePair("apikey",apikey));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairList));

                response = httpClient.execute(httpPost);
                Log.d("data", httpPost.toString());
                Log.d("response", response.toString());
                Log.d("data", httpPost.toString());
            } catch (ClientProtocolException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            }
            return response.toString();
        }
        protected void onGetExecute() {
            Toast.makeText(MainActivity.this, "success http",
                    Toast.LENGTH_LONG).show();
        }
    }
    public static void putPref(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPref(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
