import webapp2
import os
import jinja2
from datamodels import Users

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
		users = Users().query().fetch()
		template_values = {
		'users' : users
		}
		template = JINJA_ENVIRONMENT.get_template('index.html')
		self.response.write(template.render(template_values))

class Register(webapp2.RequestHandler):
	def get(self):
		template = JINJA_ENVIRONMENT.get_template('register.html')
		self.response.write(template.render())

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/register/', Register)
], debug=True)
