import webapp2
import sendgrid
from datamodels import Users
import sys
import logging

class CreateAccount(webapp2.RequestHandler):
	def post(self):
		if verify_api_key(self) == True:
			name = self.request.get('name')
			email = self.request.get('email')
			userid = self.request.get('userid')
			try:
				if '' in (name, email, userid):
					give_error_message(self, 'Create Account: One or more of the fields name, email and userid contained a blank value.')
				else:
					user = Users(id = userid, name = name, email = email)
					user.put()
					send_welcome_email(email, name)
					give_info_message(self, 'Account created')
			except:
				give_error_message(self, 'Create Account: They Python application gave an error when creating an account.')

class Connect(webapp2.RequestHandler):
	def get(self):
		if verify_api_key(self) == True:
			yourid = self.request.get('yourid')
			theirid = self.request.get('theirid')
			try:
				you = Users.get_by_id(yourid)
				them = Users.get_by_id(theirid)
				if you and them:
					status = send_connection_email(you.email, them.email, you.name, them.name)
					if status == 200:
						give_info_message(self, 'Connect: Email sent to ' + you.email + ' and ' + them.email)
					else:
						give_error_message(self, 'Connect: Sendgrid gave an error when sending an email to ' + you.email + ' and ' + them.email)
				else:
					give_error_message(self, 'One or more of the user IDs ' + yourid + " and " + theirid + " were not found in the database.")
			except:
				self.response.status = 400
				give_error_message(self, 'Connect: The Python application gave an error when sending an email to ' + you.email + ' and ' + them.email)

def send_connection_email(email1, email2, name1, name2):
	subject = "Introduction: " + name1 + " and " + name2
	body = """
Hello %s and %s,
	
Congratulations on meeting each other. Here are your contact details. We'll let you take it from here.

Happy networking,

The Barcode Conference Team
	""" % (name1, name2)
	sg = sendgrid.SendGridClient('gabriel23', 'rhal1jon')
	message = sendgrid.Mail(to=[email1,email2], subject=subject, text=body, from_email="miloharper@gmail.com")
	status, msg = sg.send(message)
	return status

def send_welcome_email(email, name):
	subject = "Welcome to Barcode Conference Scanner"
	body = """
Hello %s,
	
Welcome to Barcode Conference Scanner. When you meet people scan their barcode and we'll send you both an email with your contact details.

Happy networking,

The Barcode Conference Team
	""" % (name)
	sg = sendgrid.SendGridClient('gabriel23', 'rhal1jon')
	message = sendgrid.Mail(to=email, subject=subject, text=body, from_email="miloharper@gmail.com")
	sg.send(message)

def verify_api_key(self):
	apikey = self.request.get('apikey')
	if apikey == "GTqg08fjeb":
		return True
	else:
		self.response.status = 401
		self.response.write("Access denied.")
		return False

def give_info_message(self, message):
	self.response.write(message)
	logging.info(message)

def give_error_message(self, message):
	self.response.status = 400
	self.response.write(message)
	logging.error(message)

app = webapp2.WSGIApplication([
    ('/api/create-account/', CreateAccount),
    ('/api/connect/', Connect)
], debug=True)
